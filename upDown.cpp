//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
// Примеры строк, работы с ними, а так же элементарная арифметика
//V 1.0
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=
#include<iostream>
#include<unistd.h>
using namespace std;

int main() {
int min = 0, max = 255, upDown = 0, steps = 15, count = 0;
int iter = 2 * (max / steps);
while (count <= iter)
{
	++count;
	upDown += steps;
	if (upDown < min || upDown > max){
		steps = -steps;
	}
	cout << upDown << endl;
}
	return 0;
}
//OUTPUT
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
//END FILE
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

