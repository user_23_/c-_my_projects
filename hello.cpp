//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
//2-х мерный массив
//V 1.0
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
#include<iostream>
using namespace std;
	
int main() {
const int ARR_COL_SIZE = 3, ARR_ROW_SIZE = 3;
int multiArr[ARR_ROW_SIZE][ARR_COL_SIZE] = {{12, 30, 45},{78, 96, 101}};
cout << "доступ к элементам 2-х мерного массива обычном споссобом: ";
cout << multiArr[0][0] << ", ";//1 РЯД 1 КОЛОНКИ 
cout << multiArr[0][1] << ", ";//1 РЯД 2 КОЛОНКИ 
cout << multiArr[0][2] << ", ";//1 РЯД 3 КОЛОНКИ 
cout << multiArr[1][0] << ", ";//2 РЯД 1 КОЛОНКИ 
cout << multiArr[1][1] << ", ";//2 РЯД 2 КОЛОНКИ 
cout << multiArr[1][2];//2 РЯД 3 КОЛОНКИ 
cout << endl;
cout << "доступ к элементам 2-х мерного массива перебором (for): ";
for (int row = 0; row < 2; ++row){
	for (int col = 0; col < 3; ++col){
		cout << multiArr[row][col]<< " ";
	}
	cout << "\t";
}
cout << "\nПолучение  x/y координат\n";
cout << "X: ";
for (int x = 0; x < 3; ++x){
	cout << multiArr[0][x]<< " ";
}
cout << "\nY: ";
for (int y = 0; y< 3; ++y){
		cout << multiArr[1][y]<< " ";
}
cout << endl;

		return 0;
}
/*OUTPUT:

*/
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
//END FILE
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
